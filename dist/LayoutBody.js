function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component } from "react";
import $ from "jquery";
import { compose } from "recompose";
import { withRouter } from "react-router";
import Layouts, { cssStyle, loginPage, template } from "react-pe-layouts";
import { __ } from "react-pe-utilities";
import { title } from "react-pe-layouts";
import { isLoggedPage } from "react-pe-utilities";
import LayoutBodyLocal from "./body/LayoutBodyLocal";
import LayoutBodyStatic from "./body/LayoutBodyStatic";
import LayoutBodyExtended from "./body/LayoutBodyExtended";
class LayoutBody extends Component {
  constructor(props) {
    super(props);
    _defineProperty(this, "random", void 0);
    _defineProperty(this, "onChangeStyle", style => {
      // console.log( style );
      localStorage.setItem("css", style.style);
      localStorage.setItem("fluid", parseInt(style.fluid) ? 1 : 0);
      $("#external-css").detach();
      if (style.style) {
        // console.log( style.style, this.state.style );
        $("#under-footer").append(`<link rel="stylesheet" type="text/css" href=${style.style}?${this.random} id="external-css"/>`);
      } else {}
      // this.setState( { style } );
      // console.log( localStorage.getItem("fluid"));
    });
    _defineProperty(this, "onCurrent", i => {
      this.setState({
        current: i
      });
    });
    this.random = Math.random();
    const fl = localStorage.getItem("fluid");
    const fluid = typeof fl !== "undefined" ? fl : 1;
    const _style = {
      style: cssStyle(),
      fluid
    };
    document.title = title();
    const token = localStorage.getItem("token");
    if (!token && isLoggedPage(props.location.pathname)) {
      this.props.history.push(loginPage());
    }
    this.state = {
      current: 100,
      style: _style,
      fluid
    };
    //
  }

  render() {
    const cl = this.props.location.pathname.split("/").splice(1).map(e => `route-${e}`).join(" ");
    const clss = this.state.style && this.state.style.fluid ? "container-fluid  cont" : "container cont";
    let content = "";
    switch (JSON.stringify(Layouts().app.init_method)) {
      case "static":
        content = /*#__PURE__*/React.createElement(LayoutBodyStatic, _extends({}, this.props, this.state, {
          onCurrent: this.onCurrent,
          onChangeStyle: this.onChangeStyle
        }));
        break;
      case "local":
        content = /*#__PURE__*/React.createElement(LayoutBodyLocal, _extends({}, this.props, this.state, {
          onCurrent: this.onCurrent,
          onChangeStyle: this.onChangeStyle
        }));
        break;
      case "external":
      default:
        content = /*#__PURE__*/React.createElement(LayoutBodyExtended, _extends({}, this.props, this.state, {
          onCurrent: this.onCurrent,
          onChangeStyle: this.onChangeStyle
        }));
    }
    return /*#__PURE__*/React.createElement("div", {
      className: `full ${cl}`
    }, /*#__PURE__*/React.createElement("header", null), /*#__PURE__*/React.createElement("main", null, /*#__PURE__*/React.createElement("div", {
      className: clss
    }, content)), /*#__PURE__*/React.createElement("footer", null), /*#__PURE__*/React.createElement("div", {
      id: "under-footer"
    }, /*#__PURE__*/React.createElement("link", {
      href: `/assets/css/style.css?${this.random}`,
      rel: "stylesheet"
    }), /*#__PURE__*/React.createElement("link", {
      rel: "stylesheet",
      type: "text/css",
      href: `${cssStyle()}?${this.random}`,
      id: "external-css"
    })));
  }
}
export default compose(withRouter)(LayoutBody);