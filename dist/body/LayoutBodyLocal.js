function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component } from "react";
import { initWidgets } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";
import { cssStyle, template } from "react-pe-layouts";
import LayoutContent from "react-pe-layout-app";
import LayoutFooter from "react-pe-layout-app";
import LayoutHeader from "react-pe-layout-app";
class LayoutBodyLocal extends Component {
  constructor(...args) {
    super(...args);
    _defineProperty(this, "state", {});
    _defineProperty(this, "random", void 0);
    _defineProperty(this, "user", {
      id: -1,
      roles: []
    });
    _defineProperty(this, "refetch", () => {});
  }
  componentDidMount() {
    initWidgets();
  }
  render() {
    const user = {
      id: -1,
      roles: []
    };
    return /*#__PURE__*/React.createElement(React.Fragment, null, initArea("layout-app", {
      ...this.props,
      user,
      refetchUser: this.refetch
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout block w-100"
    }, !template().header ? null : initArea("layout-header", {
      ...this.props,
      user,
      refetchUser: this.refetch,
      onCurrent: this.onCurrent
    }, /*#__PURE__*/React.createElement(LayoutHeader, {
      name: this.props.name,
      current: this.state.current,
      onCurrent: this.onCurrent,
      user: user,
      refetchUser: this.refetch
    })), /*#__PURE__*/React.createElement(LayoutContent, {
      current: this.state.current,
      onCurrent: this.onCurrent,
      user: user,
      onChangeStyle2: style => this.onChangeStyle(cssStyle()),
      onChangeStyle: this.onChangeStyle,
      refetchUser: this.refetch
    }), /*#__PURE__*/React.createElement(LayoutFooter, null))));
  }
}
export default LayoutBodyLocal;