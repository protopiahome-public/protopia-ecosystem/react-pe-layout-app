function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component } from "react";
import LayoutMenuLeft from "./LayoutMenuLeft";
import LayoutMain from "./LayoutMain";
import LayoutMenuMain from "./LayoutMenuMain";
import { template, isMenuLeft } from "react-pe-layouts";
import LayoutServices from "./LayoutServices";
class LayoutContent extends Component {
  constructor(props) {
    super(props);
    _defineProperty(this, "onChangeStyle", style => {
      if (typeof style.isLeft != "undefined" && style.isLeft !== this.state.isLeft) {
        // console.log(style.isLeft)
        this.setState({
          isLeft: style.isLeft
        });
      }
      // console.log( style );
      this.props.onChangeStyle(style);
    });
    this.state = {
      current: this.props.current,
      isLeft: isMenuLeft()
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.current !== this.state.current) {
      this.setState({
        current: nextProps.current
      });
    }
  }
  render() {
    // console.log(this.state.isLeft);
    const menu_left = this.state.isLeft ? /*#__PURE__*/React.createElement(LayoutMenuLeft, {
      current: this.state.current,
      onCurrent: this.props.onCurrent,
      user: this.props.user,
      refetchUser: this.props.refetchUser
    }) : null;
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-content "
    }, menu_left, /*#__PURE__*/React.createElement(LayoutMenuMain, {
      current: this.state.current,
      onCurrent: this.props.onCurrent,
      user: this.props.user
    }), /*#__PURE__*/React.createElement(LayoutMain, {
      current: this.state.current,
      onChangeStyle: this.onChangeStyle,
      user: this.props.user
    }), /*#__PURE__*/React.createElement(LayoutServices, {
      current: this.state.current,
      onCurrent: this.props.onCurrent,
      user: this.props.user
    }));
  }
}
export default LayoutContent;