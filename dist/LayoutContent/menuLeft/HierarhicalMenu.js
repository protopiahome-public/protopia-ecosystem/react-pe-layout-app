function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { concatRouting } from "react-pe-layouts";
import { __ } from "react-pe-utilities";
class HierarhicalMenu extends Component {
  constructor(...args) {
    super(...args);
    _defineProperty(this, "state", {
      current: this.props.current,
      hover: false
    });
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      current: nextProps.current
    });
  }
  render() {
    const {
      children,
      parent_route,
      route,
      title,
      level,
      razdel
    } = this.props;
    const myRoute = `${parent_route}/${route}`;
    const parents = children && children.length > 0 ? children.map((e, i) => /*#__PURE__*/React.createElement(HierarhicalMenu, _extends({
      level: level + 1,
      parent_route: myRoute
    }, e, {
      razdel: razdel,
      i: i,
      key: i
    }))) : null;
    const btn = this.props.is_hidden ? null : /*#__PURE__*/React.createElement(NavLink, {
      to: myRoute,
      exact: true,
      className: "layout-left-btn ",
      activeClassName: "active",
      i: this.props.current
    }, /*#__PURE__*/React.createElement("div", {
      className: `layout-menu-left-label ${this.state.hover ? "hover" : null}`,
      style: {
        paddingLeft: level * 20
      }
    }, __(title)));
    return /*#__PURE__*/React.createElement("div", {
      className: `layout-left-group ${route}`
    }, btn, parents);
  }
  getRoutes() {
    return this.props.razdel ? this.props.razdel : [];
  }
  insertRoute(level = 1, n = 0) {
    //const urls = this.getRoutes()
    return `${this.props.parent_route}/${this.props.route}`;
    /*
    let route = "";
    for(let i = 0; i < level ; i++)
    {
    console.log( urls );
    if( urls[i] )
    {
    	//route = urls[i] ? route + "/" + urls[i].route : route;
    	route = route + "/" + urls[i].route;
    }
    else
    {
    	route = route;
    }
    }
    //console.log( route );
    return route;
    */
  }

  getFirstRoute() {
    const url = this.getRoutes()[1];
    return url || "";
  }
  getParent() {
    const rts = this.getFirstRoute();
    // console.log(rts);

    let routing = [];
    routing = concatRouting();
    return routing.filter(e => e.route === rts);
  }
  getGrandChildren(chldrn) {
    if (!chldrn) return false;
    // console.log(chldrn);
    if (chldrn.children && chldrn.children.length > 0) {
      return chldrn.children;
    }
    return false;
  }
  getChildren() {
    const chldrn = this.getParent();
    if (chldrn.length > 0) {
      // console.log( chldrn[0].children );
      if (chldrn[0].children && chldrn[0].children.length > 0) {
        return chldrn[0].children;
      }
      return false;
    }
    return false;
  }
}
export default withRouter(HierarhicalMenu);