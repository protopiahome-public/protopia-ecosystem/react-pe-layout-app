function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
import React, { Component, Suspense } from "react";
import { compose } from "recompose";
import { Redirect, Route, Switch, withRouter } from "react-router";
import { concatRouting, mainPage, routeData } from "react-pe-layouts";
import { isCapability } from "react-pe-utilities";
import Layouts from "react-pe-layouts";
import { __, importAll } from "react-pe-utilities";
import { initArea } from "react-pe-utilities";
import { Loading } from "react-pe-useful";

/* ================================
**
** TODO: включить lazy-loading
**
================================ */

const components = {};
const lazies = {};
const plViews = [];
class LayoutMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false
    };
  }
  componentDidMount() {
    // Все Компоненты из папки states для последующей подстановки в роутинги
    importAll(require.context("states/", false, /\.js$/), [], components, lazies, "states/");

    // Собираем все компоненты из папки modules (которые указаны в layouts.json, раздел modules)
    // для последующей подстановки в роутинги
    const plgns = Layouts().modules;
    Object.keys(plgns).forEach(plugin => {
      Object.keys(plgns[plugin].views).forEach(view => {
        plViews.push(view);
      });
    });
    importAll(require.context("modules/", true, /\.js$/), plViews, components, lazies, "modules/");
    setTimeout(() => {
      this.setState({
        isLoaded: true
      });
    }, 30);
  }
  render() {
    if (!this.state.isLoaded) {
      return /*#__PURE__*/React.createElement(Loading, null);
    }
    const main = mainPage();
    let routing = [];
    routing = concatRouting();
    const routers = [];
    const overs = [];
    const grands = [];
    //const grandgrands = []
    // console.log(routing);
    routing.forEach((e, i) => {
      if (e.children && e.children.length > 0) {
        e.children.forEach((elem, n) => {
          if (elem.children && elem.children.length > 0) {
            elem.children.forEach((element, nn) => {
              grands.push(this.searchRouteData(nn, e, elem, element));
            });
          }
          overs.push(this.searchRouteData(n, e, elem));
        });
      }
      routers.push(this.searchRouteData(i, e));
    });
    const NoMatchView = components.NoMatchView.default;
    const SettingsView = components.SettingsView ? components.SettingsView.default : NoMatchView;
    const RegisterView = components.RegisterView ? components.RegisterView.default : NoMatchView;
    const LoginView = components.LoginView ? components.LoginView.default : NoMatchView;
    const RememberPasswordView = components.RememberPasswordView ? components.RememberPasswordView.default : NoMatchView;
    const ResetPasswordView = components.ResetPasswordView ? components.ResetPasswordView.default : NoMatchView;
    const FinishRestorePasswordView = components.FinishRestorePasswordView ? components.FinishRestorePasswordView.default : NoMatchView;
    const ChangeEmailView = components.ChangeEmailView ? components.ChangeEmailView.default : NoMatchView;
    const VerifyUserView = components.VerifyUserView ? components.VerifyUserView.default : NoMatchView;
    const SearchView = components.SearchView ? components.SearchView.default : NoMatchView;
    return /*#__PURE__*/React.createElement(Switch, null, this.searchRouteData(0, main, null, null, ""), grands, overs, routers, /*#__PURE__*/React.createElement(Route, {
      path: "/login/",
      strict: false,
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(LoginView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "login"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/register/",
      strict: false,
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(RegisterView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "register"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/remember/",
      strict: false,
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(RememberPasswordView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "remember"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/cog/",
      strict: false,
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(SettingsView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "cog"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/reset/",
      strict: false,
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(ResetPasswordView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "reset"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/restore/:id/:code",
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(FinishRestorePasswordView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "restore"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/verify/:id/:code",
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(VerifyUserView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "verify"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/search/",
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(SearchView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "search"
    }), /*#__PURE__*/React.createElement(Route, {
      path: "/changeemail/:id/:code",
      exact: true,
      component: routeProps => /*#__PURE__*/React.createElement(ChangeEmailView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      }),
      key: "changeemail"
    }), initArea("main-routes", {
      ...this.props
    }, /*#__PURE__*/React.createElement(Route, {
      component: routeProps => /*#__PURE__*/React.createElement(NoMatchView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      })
    })), /*#__PURE__*/React.createElement(Route, {
      path: "*",
      component: routeProps => /*#__PURE__*/React.createElement(NoMatchView, {
        onChangeStyle: style => this.props.onChangeStyle(style),
        user: this.props.user
      })
    }), /*#__PURE__*/React.createElement(Redirect, {
      to: "/404"
    }));
  }

  /*
    @i - (string | int) key index
    @e - (object)- layouts.json element (in menu, profile, extended_routes, bells, comments, help)
    @child - (object) child of @e
    @grandchild - (object) child of @child
    @forceRoute - routee's URL forced up the object.route
    return Route
  */
  searchRouteData(i, e = undefined, child = undefined, grandchild = undefined, forceRoute = undefined) {
    const routeData1 = routeData(e, child, grandchild, forceRoute);
    if (isCapability(routeData1.capability, this.props.user)) {} else {
      try {
        let rt = this.returnedRoute(i, routeData1.currentE, routeData1.preroute, routeData1.route, routeData1.noexact_route);
        return rt;
      } catch (errpr) {
        console.log(errpr);
        console.log("e", e);
        console.log("child", child);
        console.log("grandchild", grandchild);
        console.log("forceRoute", forceRoute);
        return null;
      }
    }
  }
  returnedRoute(i, currentE, preroute, route, noexact_route) {
    // const is = `${preroute}/${route}` === this.props.location.pathname
    //   || `${preroute}/${route}/` === this.props.location.pathname

    const NoMatchView = components.NoMatchView.default;
    const DataTableView = components.DataTableView ? components.DataTableView.default : NoMatchView;
    const DataView = components.DataView ? components.DataView.default : NoMatchView;
    const HTMLView = components.HTMLView ? components.HTMLView.default : NoMatchView;
    const HTMLSourceVew = components.HTMLSourceVew ? components.HTMLSourceVew.default : NoMatchView;
    const SingledDataTypeView = components.SingledDataTypeView ? components.SingledDataTypeView.default : NoMatchView;
    const FeedDataTypeView = components.FeedDataTypeView ? components.FeedDataTypeView.default : NoMatchView;

    //console.log( currentE )
    if (currentE.html_source) {
      return /*#__PURE__*/React.createElement(Route, {
        exact: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(HTMLSourceVew, _extends({}, currentE, {
          onChangeStyle: style => this.props.onChangeStyle(style)
        })),
        key: i
      });
    } else if (currentE.html) {
      return [/*#__PURE__*/React.createElement(Route, {
        exact: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(HTMLView, _extends({}, currentE, {
          onChangeStyle: style => this.props.onChangeStyle(style)
        })),
        key: `${i}_2`
      })];
    } else if (currentE.redirect) {
      return [/*#__PURE__*/React.createElement(Route, {
        exact: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(Redirect, {
          to: currentE.redirect
        }),
        key: `${i}_2`
      })];
    } else if (currentE.singled_data_type) {
      const schemaElement = Layouts().schema[currentE.singled_data_type];
      const SingleComponento = schemaElement && schemaElement.view ? components[schemaElement.view].default : SingledDataTypeView;
      return [/*#__PURE__*/React.createElement(Route, {
        strict: true,
        path: `${preroute}/${noexact_route}`,
        render: routeProps => /*#__PURE__*/React.createElement(SingleComponento, _extends({}, currentE, {
          path: `${preroute}/${noexact_route}`,
          route: `${preroute}/${route}`,
          onChangeStyle: style => this.props.onChangeStyle(style),
          user: this.props.user
        })),
        key: `${i}_2`
      })];
    } else if (currentE.feed_data_type) {
      return [/*#__PURE__*/React.createElement(Route, {
        strict: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(FeedDataTypeView, _extends({}, currentE, {
          route: `${preroute}/${route}`,
          onChangeStyle: style => this.props.onChangeStyle(style),
          user: this.props.user
        })),
        key: `${i}_2`
      })];
    } else if (currentE.single_data_type) {
      // console.log( currentE );
      // console.log( preroute + '/' +  route );
      return /*#__PURE__*/React.createElement(Route, {
        strict: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(DataView, _extends({}, currentE, {
          preroute: preroute,
          route: `${preroute}/${route}`,
          onChangeStyle: style => this.props.onChangeStyle(style)
        })),
        key: `${i}_2`
      });
    } else if (currentE.data_type) {
      // console.log( currentE.route);

      return [/*#__PURE__*/React.createElement(Route, {
        strict: true,
        path: `${preroute}/${noexact_route}`,
        render: routeProps => /*#__PURE__*/React.createElement(DataView, _extends({}, currentE, {
          route: `${preroute}/${route}`,
          onChangeStyle: style => this.props.onChangeStyle(style)
        })),
        key: `${i}_2`
      }), /*#__PURE__*/React.createElement(Route, {
        exact: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(DataTableView, _extends({}, currentE, {
          route: `${preroute}/${route}`,
          onChangeStyle: style => this.props.onChangeStyle(style)
        })),
        key: `${i}_1`
      })];
    } else if (currentE.component) {
      try {
        const ElComponento = components[currentE.component].default;
        //const ElComponento = allComponents[currentE.component]

        return [/*#__PURE__*/React.createElement(Route, {
          strict: true,
          path: `${preroute}/${noexact_route}`,
          component: routeProps => /*#__PURE__*/React.createElement(Suspense, {
            fallback: /*#__PURE__*/React.createElement(Loading, null)
          }, /*#__PURE__*/React.createElement(ElComponento, _extends({}, currentE, {
            preroute: preroute,
            onChangeStyle: style => this.props.onChangeStyle(style),
            user: this.props.user
          }))),
          key: `${i}_1`
        }), /*#__PURE__*/React.createElement(Route, {
          exact: true,
          path: `${preroute}/${route}`,
          render: routeProps => /*#__PURE__*/React.createElement(Suspense, {
            fallback: /*#__PURE__*/React.createElement(Loading, null)
          }, /*#__PURE__*/React.createElement(ElComponento, _extends({}, currentE, {
            preroute: preroute,
            onChangeStyle: style => this.props.onChangeStyle(style),
            user: this.props.user
          }))),
          key: `${i}_2`
        })];
      } catch (e) {
        return [/*#__PURE__*/React.createElement(Route, {
          exact: true,
          path: `${preroute}/${route}`,
          render: routeProps => /*#__PURE__*/React.createElement(NoMatchView, {
            title: __("No exists Component: ") + currentE.component,
            icon: "",
            style_id: currentE.style_id,
            is_left: currentE.is_left,
            onChangeStyle: style => this.props.onChangeStyle(style)
          }),
          key: i
        })];
      }
    } else {
      return [/*#__PURE__*/React.createElement(Route, {
        exact: true,
        path: `${preroute}/${route}`,
        render: routeProps => /*#__PURE__*/React.createElement(NoMatchView, {
          title: "- 404 -",
          icon: "",
          style_id: currentE.style_id,
          is_left: currentE.is_left,
          onChangeStyle: style => this.props.onChangeStyle(style)
        }),
        key: i
      })];
    }
  }
}
export default compose(withRouter)(LayoutMain);