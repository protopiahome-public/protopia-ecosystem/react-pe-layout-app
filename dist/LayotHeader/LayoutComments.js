function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { LayoutIcon } from 'react-pe-useful';
import { getFirstRoute } from "react-pe-layouts";
class LayoutComments extends Component {
  constructor(...args) {
    super(...args);
    _defineProperty(this, "state", {});
  }
  render() {
    const route = getFirstRoute("comments");
    return /*#__PURE__*/React.createElement("div", {
      className: " "
    }, /*#__PURE__*/React.createElement("div", {
      className: " "
    }, /*#__PURE__*/React.createElement(NavLink, {
      to: route.route
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      src: route.icon,
      className: "layout-header-icon"
    }))));
  }
}
export default LayoutComments;