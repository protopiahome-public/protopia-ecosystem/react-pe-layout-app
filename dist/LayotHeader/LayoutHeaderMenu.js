function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component, Fragment } from "react";
import { NavLink } from "react-router-dom";
import { Popover, Menu, MenuItem, Button } from "@blueprintjs/core";
import { compose } from "recompose";
import { withRouter } from "react-router";
import { __ } from "react-pe-utilities";
import { mainMenu } from "react-pe-layouts";
import { isCapability } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
class LayoutHeaderMenu extends Component {
  constructor(...args) {
    super(...args);
    _defineProperty(this, "onRoute", evt => {
      const route = evt.currentTarget.getAttribute("route");
      this.props.history.push(route);
      this.props.onHumburger();
    });
  }
  render() {
    const mainMenu1 = mainMenu().map((e, i) => {
      const isRole = isCapability(e.capability, this.props.user);
      if (e.children && e.children.length > 0) {
        const podmenu = [];
        const children = e.children.map((ee, ii) => {
          const rt = `/${e.route}/${ee.route}`;
          // console.log( this.props.location.pathname, rt);
          podmenu.push( /*#__PURE__*/React.createElement("div", {
            className: `${this.props.location.pathname === rt ? "active " : ""}podmenu`,
            key: ii,
            route: rt,
            onClick: this.onRoute
          }, __(ee.title)));
          return /*#__PURE__*/React.createElement(MenuItem, {
            key: ii,
            text: __(ee.title),
            route: rt,
            onClick: this.onRoute,
            active: this.props.location.pathname === rt
          });
        });
        if (isRole) return "";
        return /*#__PURE__*/React.createElement(Fragment, {
          key: i + 1000
        }, /*#__PURE__*/React.createElement(Popover, {
          popoverClassName: "p-0 menu-popover",
          className: "p-0",
          position: "bottom-left",
          usePortal: false,
          enforceFocus: true,
          interactionKind: "hover",
          content: /*#__PURE__*/React.createElement(Menu, {
            key: "menu"
          }, children)
        }, /*#__PURE__*/React.createElement(NavLink, {
          to: {
            pathname: `/${e.route}`
          },
          exact: true,
          route: e.route,
          onClick: this.onRoute,
          activeClassName: "active"
        }, /*#__PURE__*/React.createElement("span", null, __(e.title), /*#__PURE__*/React.createElement("span", {
          className: "ml-2"
        }, /*#__PURE__*/React.createElement("svg", {
          xmlns: "http://www.w3.org/2000/svg",
          width: "11",
          height: "11",
          x: "0px",
          y: "0px",
          viewBox: "0 0 496.135 496.135"
        }, /*#__PURE__*/React.createElement("path", {
          fill: "#FFFFFF",
          d: "M443.5 162.6l-7.1-7.1c-4.7-4.7-12.3-4.7-17 0L224 351 28.5 155.5c-4.7-4.7-12.3-4.7-17 0l-7.1 7.1c-4.7 4.7-4.7 12.3 0 17l211 211.1c4.7 4.7 12.3 4.7 17 0l211-211.1c4.8-4.7 4.8-12.3.1-17z"
        })))))), podmenu);
      }
      if (isRole) return "";
      return /*#__PURE__*/React.createElement(NavLink, {
        route: e.route,
        onClick: this.onRoute,
        to: {
          pathname: `/${e.route}`
        },
        exact: true,
        activeClassName: "active",
        key: i
      }, /*#__PURE__*/React.createElement("div", {
        className: "header-menu-element"
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: e.icon,
        className: "header-menu-icon"
      }), /*#__PURE__*/React.createElement("span", null, __(e.title))));
    });
    return /*#__PURE__*/React.createElement("div", {
      className: "main-menu"
    }, mainMenu1);
  }
  f() {
    return /*#__PURE__*/React.createElement("div", {
      className: "main-menu"
    }, /*#__PURE__*/React.createElement(Popover, {
      popoverClassName: "p-0",
      className: "p-0",
      position: "bottom-left",
      usePortal: false,
      enforceFocus: true,
      interactionKind: "hover",
      content: /*#__PURE__*/React.createElement(Menu, {
        key: "menu"
      }, /*#__PURE__*/React.createElement(MenuItem, {
        text: "\u041C\u043E\u0438 \u0441\u043E\u0431\u044B\u0442\u0438\u044F"
      }), /*#__PURE__*/React.createElement(MenuItem, {
        text: "\u041F\u043E\u0438\u0441\u043A"
      }), /*#__PURE__*/React.createElement(MenuItem, {
        text: "\u0410\u0440\u0445\u0438\u0432 \u0441\u043E\u0431\u044B\u0442\u0438\u0439"
      }))
    }, /*#__PURE__*/React.createElement(NavLink, {
      to: {
        pathname: "/affiche"
      },
      exact: true,
      rightIcon: "caret-down"
    }, /*#__PURE__*/React.createElement("span", null, __("Афиша событий")))), /*#__PURE__*/React.createElement(NavLink, {
      to: {
        pathname: "/map"
      },
      exact: true,
      text: "\u041A\u0430\u0440\u0442\u0430",
      activeClassName: "active"
    }, /*#__PURE__*/React.createElement("span", null, __("Map"))), /*#__PURE__*/React.createElement(Button, {
      text: "\u041C\u043E\u0439 \u043A\u0430\u0431\u0438\u043D\u0435\u0442",
      minimal: true
    }));
  }
}
export default compose(withRouter)(LayoutHeaderMenu);