function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { NavLink } from "react-router-dom";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { profile } from "react-pe-layouts";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import { isCapability } from "react-pe-utilities";
import { login, template } from "react-pe-layouts";
import { initArea } from "react-pe-utilities";
import UserContext from "../layoutConfig/userContext";
const components = {};
function importAll(r) {
  // console.log(r)
  r.keys().forEach(key => {
    const key1 = key.replace("./", "").split(".").slice(0, -1).join(".");
    components[key1] = r(key);
  });
}
importAll(require.context("widgets/", false, /\.js$/));
class LayoutUser extends Component {
  constructor(props) {
    super(props);
    _defineProperty(this, "onMouseLeaveHandler", e => {
      //const domNode = ReactDOM.findDOMNode(this)
      const domNode = this.myRef.current;
      if (!domNode || !domNode.contains(e.target)) {
        this.setState({
          isOpen: this.props.isOpen,
          height: 0
        });
      }
    });
    _defineProperty(this, "onToggle", evt => {
      // console.log( document.getElementById("person_menu").clientHeight );
      this.setState({
        isOpen: !this.state.isOpen,
        height: !this.state.isOpen ? document.getElementById("person_menu").clientHeight : 0
      });
    });
    _defineProperty(this, "logout", (evt, context) => {
      //console.log(context);
      context.setUser({});
      localStorage.removeItem("token");
      this.props.refetchUser();
    });
    this.state = {
      isOpen: this.props.isOpen,
      height: 0,
      current: this.props.current
    };
    this.myRef = /*#__PURE__*/React.createRef();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.current !== this.state.current || nextProps.isOpen !== this.state.isOpen) {
      this.setState({
        current: nextProps.current,
        isOpen: nextProps.isOpen,
        height: nextProps.isOpen ? 230 : 0
      });
    }
  }
  componentDidMount() {
    document.body.addEventListener("click", this.onMouseLeaveHandler);
  }
  componentWillUnmount() {
    document.body.removeEventListener("click", this.onMouseLeaveHandler);
  }
  render() {
    // console.log(this.props.user);
    return /*#__PURE__*/React.createElement(React.Fragment, null, initArea("before-profile", {
      ...this.props
    }), /*#__PURE__*/React.createElement(UserContext.Consumer, null, context => {
      //console.log( context )
      return context.user && Object.entries(context.user).length > 0 ? this.logined(context) : this.unLogined(context);
    }));
  }
  unLogined() {
    // console.log(template());
    return /*#__PURE__*/React.createElement(React.Fragment, null, initArea("unlogin_panel", {
      ...this.props
    }, /*#__PURE__*/React.createElement(NavLink, {
      className: "icon-unlogined",
      to: login
    }, /*#__PURE__*/React.createElement("div", null, " ", __("Enter"), " "))));
  }
  logined(context) {
    const {
      avatar
    } = this.props;
    const {
      user
    } = context; //this.props
    //console.log( user )
    const profile_routing = profile();
    let profile_menu;
    if (profile_routing.length > 0) {
      profile_menu = profile_routing.map((e, i) => {
        const isRole = isCapability(e.capability, this.props.user);
        if (isRole) return "";
        let children;
        if (Array.isArray(e.children) && e.children.length > 0) {
          children = /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
            className: "lmenu-children"
          }));
        } else {}
        return /*#__PURE__*/React.createElement("li", {
          className: "lmenu",
          key: i
        }, children, /*#__PURE__*/React.createElement(NavLink, {
          className: "",
          activeClassName: "active",
          to: `/${e.route}`
        }, /*#__PURE__*/React.createElement(LayoutIcon, {
          isSVG: true,
          src: e.icon,
          className: "personal-menu__icon mr-3"
        }), __(e.title)));
      });
    } else {
      profile_menu = /*#__PURE__*/React.createElement("li", {
        className: "lmenu"
      }, /*#__PURE__*/React.createElement(NavLink, {
        className: "",
        activeClassName: "active",
        to: "/profile"
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        isSVG: true,
        src: "/assets/img/user.svg",
        className: "personal-menu__icon mr-3"
      }), __("edit profile")));
    }
    const login_panel = template().login_panel && Array.isArray(template().login_panel) ? template().login_panel.map((e, i) => {
      switch (e.component) {
        case "NavLink":
          //console.log(e)
          return /*#__PURE__*/React.createElement(NavLink, {
            key: i,
            className: `btn ${e.routing}`,
            to: `/${e.routing}`
          }, __(e.title));
        default:
          const ElWidgetL = components[e.component].default;
          return /*#__PURE__*/React.createElement(ElWidgetL, _extends({
            key: i
          }, e, {
            user: context.use,
            logout: evt => this.logout(evt, context)
          }));
      }
    }) : /*#__PURE__*/React.createElement("div", {
      className: `icon-logined ${this.state.isOpen ? " active44" : ""}`,
      onClick: this.onToggle,
      ref: this.myRef
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      isSVG: !(user.avatar && user.avatar.length > 0),
      src: user.avatar ? user.avatar : "/assets/img/user1.svg",
      style: user.avatar ? {
        backgroundImage: `url(${user.avatar})`,
        backgroundSize: "cover"
      } : {},
      className: "user-ava"
    }), /*#__PURE__*/React.createElement("div", {
      className: "user-name"
    }, user ? user.display_name : " "), /*#__PURE__*/React.createElement("div", {
      className: "chevron-down-light ml-3 mt-1 pointer"
    }), /*#__PURE__*/React.createElement("div", {
      className: "logined-menu",
      style: {
        height: this.state.height
      }
    }, /*#__PURE__*/React.createElement("ul", {
      id: "person_menu"
    }, profile_menu, /*#__PURE__*/React.createElement("li", {
      onClick: evt => this.logout(evt, context),
      className: "lmenu exit"
    }, /*#__PURE__*/React.createElement(LayoutIcon, {
      isSVG: true,
      src: "/assets/img/logout.svg",
      className: "personal-menu__icon mr-3"
    }), __("logout")))));
    return /*#__PURE__*/React.createElement(React.Fragment, null, initArea("login_panel", {
      ...this.props
    }, login_panel));
  }
}
export default compose(withApollo, withRouter)(LayoutUser);