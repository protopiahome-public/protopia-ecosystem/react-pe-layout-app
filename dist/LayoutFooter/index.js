import React from "react";
import { initArea } from "react-pe-utilities";
class LayoutFooter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return /*#__PURE__*/React.createElement("footer", {
      className: "layout-footer"
    }, initArea("layout-footer", {
      ...this.props
    }));
  }
}
export default LayoutFooter;