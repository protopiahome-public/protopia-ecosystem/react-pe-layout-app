import React, { Component, Fragment } from "react"
import { NavLink } from "react-router-dom"

import { existRoutingChilder, getFirstRoute } from "react-pe-layouts"
import {LayoutIcon} from 'react-pe-useful'
import { __ } from "react-pe-utilities"

class LayoutHelp extends Component {
	state = { }

	render() {
	  const route = getFirstRoute("help")
	  const children = existRoutingChilder("help")
	    ?			getFirstRoute("help").children.map((ee, i) => {
	      const rt = `/${route.route}/${ee.route}`
	      return (
  <NavLink
    to={rt}
    className="podmenu"
    activeClassName="active"
    key={i}
    route={rt}
  >
    { __(ee.title) }
  </NavLink>
	      )
	    })
	    :			null
	  return (
  <>
    <NavLink
      to={`/${route.route}`}
      className="layout-header-help"
    >
      <LayoutIcon
        src={route.icon}
        className="layout-header-icon"
      />
      <span>{ route.title }</span>
    </NavLink>
    {children}
  </>
	  )
	}
}

export default LayoutHelp
