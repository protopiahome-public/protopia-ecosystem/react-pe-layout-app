import React, { Component, Fragment } from "react" 
import { link } from "react-pe-layouts"
import {LayoutIcon} from 'react-pe-useful'

class LayoutLinks extends Component {
	state = { }

	render() {
	  return link().map((e, i) => (
  <a href={e.route} target="_blank" key={i} title={e.title} className="layout-header-link" rel="noreferrer">
    <LayoutIcon
      src={e.icon}
      className="layout-header-icon"
    />
    <span>
      {e.title}
    </span>
  </a>
	  ))
	}
}

export default LayoutLinks
