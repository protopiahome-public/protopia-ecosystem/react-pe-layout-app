import React, { Component } from "react"
import { initWidgets } from "react-pe-utilities" 
import { initArea } from "react-pe-utilities"
import { cssStyle, template } from "react-pe-layouts" 
import LayoutContent from "react-pe-layout-app"
import LayoutFooter from "react-pe-layout-app"
import LayoutHeader from "react-pe-layout-app"

class LayoutBodyLocal extends Component
{
    state = { }
    random;
	user = { id:-1, roles: [] }
	refetch = () => {}
	componentDidMount()
    {
		initWidgets() 
    }
    render()
    {
        const user = { id: -1, roles: [] }  
		return <> 
		{
			initArea(
				"layout-app",
				{
					...this.props,
					user,
					refetchUser: this.refetch
				},
				<div className="layout block w-100">
					{
						!template().header 
							? 
							null
							: 
							initArea(
								"layout-header",
								{
									...this.props,
									user,
									refetchUser: this.refetch,
									onCurrent: this.onCurrent,
								},
								<LayoutHeader
									name={this.props.name}
									current={this.state.current}
									onCurrent={this.onCurrent}
									user={user}
									refetchUser={this.refetch}
								/>,
							) 
					}
					<LayoutContent
						current={this.state.current}
						onCurrent={this.onCurrent}
						user={user}
						onChangeStyle2={(style) => this.onChangeStyle(cssStyle())}
						onChangeStyle={this.onChangeStyle}
						refetchUser={this.refetch}
					/>
					<LayoutFooter />
				</div>
			)
		}
		</>
    }
}
export default LayoutBodyLocal